> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Derek Piccininni

### Assignment P#1 Requirements:

*Sub-Heading:*

1. Add JQuery data validation and regular expressions
2. Review index.jsp code


#### README.md file should include the following items:

* Screenshot of index.jsp
* Screenshot of P1 data validation
 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of Index.jsp Home*:

![AMPPS Installation Screenshot](indexjsphome.png)

*Screenshot of Invalid Data Invalidation*:

![AMPPS Installation Screenshot](P1red.png)

*Screenshot of Filled out Customer Form*:

![JDK Installation Screenshot](P1green.png)


