

# LIS 4368 

## Derek Piccininni

### LIS 4368 Assignment Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create BitBucket repo
    - Complete BitBucket tutorials
    - Provide git command descriptions
	


2. [A2 README.md](a2/README.md "My A2 README.md file")
- Forward engineered ERD screenshot
    - Query Screenshots
    - Screenshot of index.jsp 
  

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Forward engineered ERD screenshot
    - Links to a3.sql and a3.mwb
    - Screenshot of index.jsp a3 

3. [A4 README.md](a4/README.md "My A4 README.md file")
    - Server Side validation 
    - Screenshots of Skillsets 10,11, and 12
    - Screenshots of Server Side validation 


4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Screenshot of index.jsp
    - Screenshot data validation in P1 
    
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Server Side validation 
    - Screenshots of data insertion
    - Screenshot of SQL tables

6. [P2 README.md](p2/README.md "My P2 README.md file")
    - Server Side validation 
    - Screenshots of data insertion
    - Screenshot of displayed data


    