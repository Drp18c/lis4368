> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Derek Piccininni

### Assignment #2 Requirements:

*Sub-Heading:*

1. Set up development environment
2. Finish MySQL setup
3. Write a database servlet

#### README.md file should include the following items:

* Assessment links
* Only *one*screenshot of the query results from the following link(see screenshot below): http://localhost:9999/hello/querybook.html
 
> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*http://localhost:9999/lis4368_student_files/a2/index.jsp*:

![Android Studio Installation Screenshot](A2Site.png)



*Screenshot of :http://localhost:9999/hello/index.html*

![AMPPS Installation Screenshot](Index.png)


*Screenshot of http://localhost:9999/hello/sayhello*:

![JDK Installation Screenshot](SayHello.png)


*Screenshot of http://localhost:9999/hello/querybook.html *:

![Android Studio Installation Screenshot](QueryBook.png)


*http://localhost:9999/hello/querybook.html *:

![Android Studio Installation Screenshot](SQ.png)


*http://localhost:9999/hello/querybook.html Results *:

![Android Studio Installation Screenshot](CompletedQuery.png)







