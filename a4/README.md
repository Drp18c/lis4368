> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Derek Piccininni

### Assignment #4 Requirements:

*Sub-Heading:*

1. Perform Server Side Validation
2. Provide screenshots of server side validation


#### README.md file should include the following items:

* Screenshot of empty a4 webpage
* Screenshot of a4 website with server side validation
* Screenshots of skillsets


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of empty a4 webpage*:

![AMPPS Installation Screenshot](A4SSV1.png)

*Screenshot of a4 with server side validation*:

![JDK Installation Screenshot](a4SSV.png)

*Screenshot of Skill Set 1 *:

![Android Studio Installation Screenshot](ss1.png)

*Screenshot of Skill Set 2 *:

![Android Studio Installation Screenshot](a4ss2.png)

*Screenshot of Skill Set 3 *:

![Android Studio Installation Screenshot](a4SS3.png)


