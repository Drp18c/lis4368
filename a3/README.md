> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Derek Piccininni

### Assignment #3 Requirements:

*Sub-Heading:*

1. Forward engineered pet store ERD
2. Updated index.jsp website


#### README.md file should include the following items:

* Screenshot of pet store ERD
* Screenshot of pet store ERD on index.jsp student website
* Links to sql and mwb files


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of forward engineered ERD:

![ERD](a3erdSS.png)

*Screenshot of ERD on index.jsp website*:

![ERD on website](erdonjsp1.png)

*Screenshot of populated pet table*:

![Pet table](pett.png)

*Screenshot of populated petstore table*:

![Petstore table](petstoret.png)

*Screenshot of populated customer table*:

![Customer Table](customert.png)

#### ERD and SQL links:

*Link:*
[A3.sql](docs/a3.sql "Bitbucket Station Locations")

*Link*
[A3.mwb](docs/a3.mwb "My Team Quotes Tutorial")
