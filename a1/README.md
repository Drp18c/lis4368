

# LIS 4368

## Derek Piccininni

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and BitBucket
2. Java (JSP) Server Development Installations
3. Chapter Questions (Ch.1,2)


#### README.md file should include the following items:

* Screenshot of running Java Hello
* Screenshot of local running http://localhost9999
* Git commands with short descriptions
* Bitbucket repo links:a)this assignment and b)the completed tutorial above (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init- Creates new local repository
2. git status- Lists the files you've changed
3. git add- Add one or more files
4. git commit- Commit changes
5. git push- Sends changes to remote repository 
6. git pull- Gets changes from your remote directory to your working directory
7. git checkout- To undo local changes
 

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![image](hellojava.png)


*Screenshot of running http://localhost9999; - My First App*:

![image](tomcat.png)

*Screenshot of A1 index.jsp:

![image](tomcatjava.png)
![image](tomcatsh.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Drp18c/bitbucketstationlocations/src/master/)

*Link to index.jsp:*
(http://localhost:9999/lis4368_student_files/index.jsp)
