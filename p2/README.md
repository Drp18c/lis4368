> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Derek Piccininni

### Assignment #p2 Requirements:

*Sub-Heading:*

1. Complete JSP/servlets web app using MVC framework
2. Provide create, read, update, and delete functionality
 

#### README.md file should include the following items:

* Screenshots of validation
* Screenshots of added information


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot Valid User Form Entry*:

![AMPPS Installation Screenshot](p21.png)

*Screenshot of Passed Validation*:

![JDK Installation Screenshot](p22.png)

*Screenshot of Display Customers - My First App*:

![Android Studio Installation Screenshot](p23.png)


