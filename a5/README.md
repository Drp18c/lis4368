> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Derek Piccininni

### Assignment #5 Requirements:

*Sub-Heading:*

1. Includes basic server side validation
2. Should prevent SQL injection
3. 

#### README.md file should include the following items:

* Include server-side validation from A4.
* Compiled server-side validation
* Display screenshots of pre, post-valid user form entry, as well as MySQL customer table entry


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of SS13*:

![AMPPS Installation Screenshot](SS134368.png)

*Screenshot of SS14*:

![JDK Installation Screenshot](4368SS13.png)

*Screenshot of SS15*:

![Android Studio Installation Screenshot](SS154368.png)

*Screenshot of A5 Not Submitted*:

![Android Studio Installation Screenshot](A5Filled.png)

*Screenshot of A5 Submitted*:

![Android Studio Installation Screenshot](a5submitted..png)

*Screenshot of SQL Table*:

![Android Studio Installation Screenshot](table.png)
